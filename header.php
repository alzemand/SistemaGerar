<header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index.php" class="scrollto"><img src="img/pg-logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Alunos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="aluno_cadastrar.php">Cadastrar</a>
              <a class="dropdown-item" href="aluno_consultar.php">Consultar</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Relatório Gerencial</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Instrutor
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="instrutor_cadastrar.php">Cadastrar</a>
              <a class="dropdown-item" href="instrutor_consultar.php">Consultar</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Relatório Gerencial</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Empresa
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="empresa_cadastrar.php">Cadastrar</a>
              <a class="dropdown-item" href="empresa_consultar.php">Consultar</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Relatório Gerencial</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Cadastrar</a>
              <a class="dropdown-item" href="#">Outra ação</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Algo mais aqui</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Ação</a>
              <a class="dropdown-item" href="#">Outra ação</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Algo mais aqui</a>
            </div>
          </li>
          <!-- <li><a href="#services">Serviços</a></li> -->
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->